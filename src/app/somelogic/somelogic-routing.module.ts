import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SomelogicPage } from './somelogic.page';

const routes: Routes = [
  {
    path: '',
    component: SomelogicPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SomelogicPageRoutingModule {}
