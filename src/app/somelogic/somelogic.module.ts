import { UserDevicesComponent } from './user-devices/user-devices.component';
import { AmplifyUIAngularModule } from '@aws-amplify/ui-angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SomelogicPageRoutingModule } from './somelogic-routing.module';
import { SomelogicPage } from './somelogic.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SomelogicPageRoutingModule,
    AmplifyUIAngularModule,
  ],
  declarations: [SomelogicPage, UserDevicesComponent],
})
export class SomelogicPageModule {}
