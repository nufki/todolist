import { Component, OnInit } from '@angular/core';
import Auth from '@aws-amplify/auth';
import { ModalController } from '@ionic/angular';

export class Device {
  isCurrent: boolean = false;

  constructor(
    public name: String,
    public key: String,
    public lastLogin: Date
  ) {}
}

@Component({
  selector: 'app-user-devices',
  templateUrl: './user-devices.component.html',
  styleUrls: ['./user-devices.component.scss'],
})
export class UserDevicesComponent implements OnInit {
  devices: Device[] = [];
  toggleTouchId: boolean = false;

  constructor(private modalController: ModalController) {}

  async ngOnInit() {
    Auth.currentAuthenticatedUser().then((user) => {
      console.log('USER ', user);

      const { attributes } = user;

      console.log('USER ATTRIBUTES: ', attributes);
      console.log('custom:touch-id' + attributes['custom:touch-id']);

      if (attributes['custom:touch-id'] === 'true') {
        this.toggleTouchId = true;
      } else {
        this.toggleTouchId = false;
      }

      user.getCachedDeviceKeyAndPassword(); // without this line, the deviceKey is null
      console.log('DeviceKey', user.deviceKey);

      var self = this;

      user.listDevices(5, 0, {
        onSuccess: function (result) {
          console.log('listDevices result: ', result);

          result.Devices.forEach((device) => {
            const d = new Device(
              device.DeviceAttributes[1].Value,
              device.DeviceKey,
              device.DeviceLastAuthenticatedDate
            );
            if (user.deviceKey === d.key) {
              d.isCurrent = true;
            }
            self.devices.push(d);
          });
        },

        onFailure: function (err) {
          alert(err);
        },
      });
    });
  }

  public toggleBiometrics(event) {
    console.log('toggleBiometrics: ', event);

    Auth.currentAuthenticatedUser().then(async (user) => {
      if (event.detail.checked) {
        let result = await Auth.updateUserAttributes(user, {
          'custom:touch-id': 'true',
        });
        console.log('BIOMETRICS ON: ', result);
      } else {
        let result = await Auth.updateUserAttributes(user, {
          'custom:touch-id': 'false',
        });
        console.log('BIOMETRICS ON: ', result);
      }
    });
  }
  // public async forgetDevice(device: Device) {
  //   try {
  //     const result = await Auth.forgetDevice();
  //     console.log(result);
  //   } catch (error) {
  //     console.log("Error forgetting device", error);
  //   }
  // }

  public forgetDevice(device: Device) {
    var self = this;

    Auth.currentAuthenticatedUser().then((user) => {
      user.forgetSpecificDevice(device.key, {
        onSuccess: function (result) {
          console.log('call forgetSpecificDevice: ', result);
          for (let d of self.devices) {
            if (d.key === device.key) {
              self.devices.splice(self.devices.indexOf(d), 1);
              break;
            }
          }
        },
        onFailure: function (err) {
          alert(err);
        },
      });

      // user.setDeviceStatusNotRemembered({
      //   onSuccess: function (result) {
      //     console.log("setDeviceStatusNotRemembered call result: " + result);
      //   },
      //   onFailure: function (err) {
      //     alert(err.message || JSON.stringify(err));
      //   },
      // });
      //this.devices = Object.assign({}, userDevices);
    });
  }

  close() {
    this.modalController.dismiss({
      dismissed: true,
    });
  }
}
