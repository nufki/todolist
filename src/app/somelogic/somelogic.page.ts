import { ModalController } from '@ionic/angular';
import { AlertController, Platform } from '@ionic/angular';
import { APIService } from './../API.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Auth } from 'aws-amplify';
import { Router } from '@angular/router';
import { UserDevicesComponent } from './user-devices/user-devices.component';

@Component({
  selector: 'app-somelogic',
  templateUrl: './somelogic.page.html',
  styleUrls: ['./somelogic.page.scss'],
})
export class SomelogicPage implements OnInit {
  todos: Array<any>;
  @ViewChild('form') form: NgForm;
  selected: boolean = false;
  user;

  constructor(
    private apiService: APIService,
    private alertCtrl: AlertController,
    private modalController: ModalController,
    private router: Router
  ) {}

  ngOnInit(): void {
    console.log('SomelogicPage created');
    Auth.currentAuthenticatedUser().then((user) => {
      this.user = user;
    });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    console.log('SomelogicPage destroyed');
  }

  public async showModalManageDevices() {
    const modal = await this.modalController.create({
      component: UserDevicesComponent,
    });
    modal.present();
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter');
    this.registerListeners();
  }

  ionViewDidLeave() {
    // Do actions here
    console.log('ionViewDidLeave');
  }

  async signOut() {
    try {
      await Auth.signOut();

      this.router.navigate(['start']);
    } catch (error) {
      console.log('error signing out: ', error);
    }
  }

  registerListeners() {
    console.log('registerListeners');

    Auth.currentAuthenticatedUser().then((user) => {
      console.log(user.username);

      this.apiService.ListTodos().then((evt) => {
        this.todos = evt.items;
        console.log('list todos');
      });

      this.apiService.OnCreateTodoListener(user.username).subscribe((evt) => {
        console.log('OnCreateTodoListener ', evt);
        const data = (evt as any).value.data.onCreateTodo;
        this.todos = [...this.todos, data];
      });
      this.apiService.OnUpdateTodoListener(user.username).subscribe((evt) => {
        console.log('OnUpdateTodoListener ', evt);
        const data = (evt as any).value.data.onUpdateTodo;
        console.log(data);
        this.todos.forEach((todo) => {
          if (todo.id === data.id) {
            this.todos[this.todos.indexOf(todo)] = data;
          }
        });
      });
      this.apiService.OnDeleteTodoListener(user.username).subscribe((evt) => {
        console.log('OnDeleteTodoListener ', evt);
        const data = (evt as any).value.data.onDeleteTodo;
        this.todos.forEach((todo) => {
          if (todo.id === data.id) {
            this.todos.splice(this.todos.indexOf(todo), 1);
          }
        });
      });
    });
  }

  submitNewTodo() {
    const name = this.form.value.name;
    const descn = this.form.value.description;
    const prio = this.form.value.priority;

    console.log('name: ' + name);
    console.log('descn: ' + descn);
    console.log('prio: ' + prio);

    this.apiService.CreateTodo({
      name: name,
      description: descn,
      priority: prio,
    });

    this.form.reset();
  }

  deleteTodo(index: number) {
    console.log('index: ', +index);
    console.log(this.todos[index]);
    const item = this.todos[index];

    this.apiService.DeleteTodo({
      id: item.id,
      // _version: item._version,
    });
  }

  async updateTodo(index: number) {
    const item = this.todos[index];

    const alert = await this.alertCtrl.create({
      inputs: [
        {
          name: 'name',
          type: 'text',
          value: item.name,
          placeholder: 'Placeholder 1',
        },
        {
          name: 'description',
          type: 'text',
          value: item.description,
          placeholder: 'Placeholder 2',
        },
        {
          name: 'priority',
          type: 'number',
          value: item.priority,
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirm Cancel');
          },
        },
        {
          text: 'Ok',
          handler: (data) => {
            console.log('Confirm Ok', data);
            this.apiService.UpdateTodo({
              id: item.id,
              name: data.name,
              description: data.description,
              priority: data.priority,
            });
          },
        },
      ],
    });

    await alert.present();
  }
}
