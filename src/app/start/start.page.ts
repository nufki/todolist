import { Router } from '@angular/router';
import { ChangeDetectorRef, Component, NgZone } from '@angular/core';
import { Auth } from 'aws-amplify';

import {
  onAuthUIStateChange,
  CognitoUserInterface,
  AuthState,
} from '@aws-amplify/ui-components';

@Component({
  selector: 'app-home',
  templateUrl: 'start.page.html',
  styleUrls: ['start.page.scss'],
})
export class HomePage {
  user: CognitoUserInterface | undefined;
  authState: AuthState;

  constructor(
    private ref: ChangeDetectorRef,
    private router: Router,
    private ngZone: NgZone
  ) {

  }

  ngOnInit() {
    // this.signIn();

    onAuthUIStateChange((authState, authData) => {
      this.authState = authState;
      this.user = authData as CognitoUserInterface;

      if (!this.ref['destroyed']) {
        this.ref.detectChanges();
        console.log(authState);
        if (authState === 'signedin') {
          this.ngZone.run(() => this.navigateTo('somelogic'));
        }
      }
    });
  }

  ngOnDestroy() {
    return onAuthUIStateChange;
  }

  navigateTo(url) {
    this.router.navigate([url]);
  }

  async signIn() {
    try {
      this.user = await Auth.signIn('nufki', 'Test1234**');
      console.log('user logged in: ', this.user);
      this.ngZone.run(() => this.navigateTo('somelogic'));
    } catch (error) {
      console.log('error signing in', error);
    }
  }
}
